const PubSubClient = require("./PubSubClient");

class Subdomain {
    constructor(name) {
        this.name = name;

        this.rooms = new Map();
        this.rooms.set('general', new PubSubClient(`${name}-room-general`));

        this.lifecycles = new Map();
        this.lifecycles.set('open', new PubSubClient(`${name}-lifecycle-open`));
        this.lifecycles.set('close', new PubSubClient(`${name}-lifecycle-close`));

        this.clients = 0;
    }

    room(roomName) {
        let room = this.rooms.get(roomName);
        if (!room) {
            room = new PubSubClient(`${this.name}-room-${roomName}`);
            this.rooms.set(roomName, room);
        }
        return room;
    }

    lifecycle(lifecycleName) {
        return this.lifecycles.get(lifecycleName);
    }

    clientConnected(clientId) {
        this.clients++;
        this.lifecycles.get('open').publish(clientId);
    }

    clientDisconnected(clientId) {
        this.clients--;
        this.lifecycles.get('close').publish(clientId);
    }

    destroy() {
        for (let pubsub of this.rooms.values()) {
            pubsub.destroy();
        }
        for (let pubsub of this.lifecycles.values()) {
            pubsub.destroy();
        }
    }
}

module.exports = Subdomain;