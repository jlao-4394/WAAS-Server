const projectId = require('./../../config.json').projectId;
const PubSub = require('@google-cloud/pubsub');
const pubsubClient = PubSub({projectId});

class PubSubClient {
    constructor(name) {
        pubsubClient.createTopic(name, () => {
            this.topic = pubsubClient.topic(name);

            if (this.queuedActions) {
                const doAction = (index) => {
                    const action = this.queuedActions[index];
                    if (!action) {
                        delete this.queuedActions;
                        return;
                    }

                    action().then(function () {
                        doAction(index + 1);
                    });
                };

                doAction(0);
            }
        });

        this.queuedActions = [];
    }

    publish(message) {
        if (this.topic) {
            this.topic.publish(message);
        } else {
            this.queuedActions.push(() => {
                this.topic.publish(message);
                return Promise.resolve();
            });
        }
    }

    subscribe(handler) {
        if (!this.subscription) {
            this.handlers = new Set();

            const onTopicCreated = () => new Promise((resolve) => {
                this.topic.subscribe({autoAck: true}).then((data) => {
                    this.subscription = data[0];

                    this.messageListener = ({data}) => {
                        this.handlers.forEach(function (handler) {
                            handler(data);
                        });
                    };
                    this.subscription.on('message', this.messageListener);
                    resolve();
                });
            });


            if (this.topic) {
                onTopicCreated();
            } else {
                this.queuedActions.unshift(onTopicCreated);
            }
        }

        this.handlers.add(handler);
    }

    unsubscribe(handler) {
        this.handlers.delete(handler);
    }

    destroy() {
        delete this.topic;

        if (this.subscription) {
            if (this.messageListener) {
                this.subscription.removeListener('message', this.messageListener);
            }
            this.subscription.delete();
            delete this.subscription;

            this.handlers.clear();
            delete this.handlers;
        }
    }
}

module.exports = PubSubClient;