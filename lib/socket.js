"use strict";

// const Logging = require('@google-cloud/logging');
// const loggingClient = Logging({projectId});
// const log = loggingClient.log('WAAS-Server');
// const logMetadata = {resource: {type: 'global'}};
// console.log = function (info) {
//     log.write(log.entry(logMetadata, {
//         type: 'log',
//         info
//     }));
// };
// console.error = function (info) {
//     log.write(log.entry(logMetadata, {
//         type: 'error',
//         info
//     }));
// };

const Subdomain = require('./classes/Subdomain');

const WS = require('uws').Server;
const ws = new WS({
    noServer: true,
    clientTracking: false,
    perMessageDeflate: false
});

ws.startAutoPing(10000);

const subdomains = new Map();

const onMessage = function (message) {
    this.subdomain.room('general').publish(message);
};

const deleteSubdomainTimeouts = new Map();

const onClose = function () {
    this.subdomain.clientDisconnected(this.id);

    if (this.subdomain.clients === 0) {
        deleteSubdomainTimeouts.set(this.name, setTimeout(() => {
            this.subdomain.destroy();
            subdomains.delete(this.name);
        }, 600000));
    }
};

const emitFunction = function (message) {
    if (this.readyState != 1) {
        return;
    }

    this.send(message);
};

const handleUpgrade = function (req, socket, head) {
    const domains = ['a', 'a'];
    if (domains.length !== 2) {
        socket.end('HTTP/1.1 ' + 400 + ' ' + 'Client verification failed' + '\r\n\r\n');
        return;
    }

    const name = domains[0];

    if (!subdomains.has(name)) {
        subdomains.set(name, new Subdomain(name));
    }

    ws.handleUpgrade(req, socket, head, function (socket) {
        socket.on('close', onClose);
        socket.on('message', onMessage);

        socket.id = req.url.split('?')[0].split('#')[0].split('/')[0];

        socket.name = name;
        socket.subdomain = subdomains.get(name);
        socket.subdomain.room('general').subscribe(function (message) {
            socket.send(message);
            console.log(message);
        });

        socket.subdomain.clientConnected(socket.id);

        clearTimeout(deleteSubdomainTimeouts.get(name));
        deleteSubdomainTimeouts.delete(name);
    });
};

module.exports = function (server) {
    server.on('upgrade', handleUpgrade);

    let closed;

    const handleExit = function (err) {
        if (err) {
            console.error(err);
        }

        if (closed) {
            return;
        }

        closed = true;

        server.close();
        ws.close();

        process.nextTick(function () {
            for (let subdomain of subdomains.values()) {
                subdomain.destroy();
            }
        });


        setTimeout(function () {
            process.exit();
        }, 5000);
    };

// process.once('uncaughtException', handleExit);
    process.once('SIGINT', handleExit);
    process.once('SIGTERM', handleExit);

    return ws;
};