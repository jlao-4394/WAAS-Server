'use strict';

const socket = require('./socket');

const http = require('http');
const httpServer = http.createServer(function (req, res) {
    res.writeHead(200);
    res.end();
});
httpServer.listen(3001);

let server;

if (process.env.NODE_ENV != 'production') {
    server = httpServer;
} else {
    const socketServer = http.createServer(function (req, res) {
        res.writeHead(301, {
            'Location': 'https://csgohandouts.com',
            'Cache-Control': 'public, max-age=15552000',
            'Strict-Transport-Security': 'max-age=15552000; includeSubDomains; preload',
            'X-Dns-Prefetch-Control': 'on'
        });
        res.end();
    });
    socketServer.listen(3000);

    server = socketServer;
}

const io = socket(server);